/**
 * @file        gtest_example.cpp
 * @author      FCA EMEA AD-TO
 * @copyright   Copyright CRF-FCA 2019
 * @brief       test algo.
 * @version $Id:gtest_example.cpp 000 2019-06-21 15:32:23Z  $
 */

// Bring in my package's API, which is what I'm testing

// Bring in gtest
#include <gtest/gtest.h>
#include <ros/ros.h>

  // Declare a test
  TEST(TestSuite, testCase1)
  {
    std::cout << "Hello world this is gTEST(TestSuite, testCase1)" << std::endl;
    ASSERT_EQ(0, 0);
  }

  // Declare another test
  TEST(TestSuite, testCase2)
  {
    std::cout << "Hello world this is gTEST(TestSuite, testCase2)" << std::endl;
    ASSERT_EQ(0, 0);
  }

  // Run all the tests that were declared with TEST()
  int main(int argc, char **argv)
  {
    testing::InitGoogleTest(&argc, argv);
    ros::init(argc, argv, "ut_gtest");
    return RUN_ALL_TESTS();
  }
